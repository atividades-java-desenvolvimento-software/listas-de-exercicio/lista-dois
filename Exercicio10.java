import java.util.Scanner;

public class Exercicio10 {
    public void exercicioDez(){
        Scanner sc = new Scanner(System.in);

        int A[] = new int[5];

        for (int i = 0; i < A.length; i++) {
            System.out.print("A[" + i + "]: ");
            A[i] = sc.nextInt();
        }
        
        int B[] = new int[A.length];

        for (int i = 0; i < A.length; i++) {
            B[i] = A[A.length - 1 - i];
        }

        System.out.println("Vetor invertido: ");

        for (int i = 0; i < B.length; i++) {
            System.out.print(" B[" + i + "]: " + B[i]);
        }

        sc.close();
    }
}
