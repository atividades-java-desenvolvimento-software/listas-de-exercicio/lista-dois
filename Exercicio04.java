import java.util.Scanner;

public class Exercicio04 {
    public void exercicioQuatro(){
        /*
        4. Elabore um programa que leia um vetor de 5 números digitados pelo usuário e uma 
         variável de um número n qualquer, depois mostre na tela o índice dos elementos que 
        são inferiores a n. 
        */
        Scanner sc = new Scanner(System.in);
        int vetor[] = new int[5];

        for(int i =0;i<vetor.length;i++){
            System.out.println("Informe vetor: ");
            vetor[i] = sc.nextInt();
        }
        
        System.out.println("Informe número N: ");
        int n = sc.nextInt();

        for(int i = 0;i<vetor.length;i++){
            if (vetor[i] < n) {
                System.out.println("Indíce: [" + i + "]");
            }
        }



        sc.close();
    }
}
