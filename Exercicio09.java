import java.util.Scanner;

public class Exercicio09 {
    public void exercicioNove(){
      Scanner sc = new Scanner(System.in);

        // Definindo vetores A e B
        int[] A = new int[5];
        int[] B = new int[5];
        
        // Lendo os elementos do vetor A
        System.out.println("Digite os elementos do vetor A:");
        for (int i = 0; i < A.length; i++) {
            System.out.print("A[" + i + "]: ");
            A[i] = sc.nextInt();
        }

        // Lendo os elementos do vetor B
        System.out.println("Digite os elementos do vetor B:");
        for (int i = 0; i < B.length; i++) {
            System.out.print("B[" + i + "]: ");
            B[i] = sc.nextInt();
        }
        
        int C[] = new int[5];

        for (int i = 0; i < C.length; i++) {
            if (i % 2 == 0) {
                C[i] = A[i];
            }else{
                C[i] = B[i];
            }
        }

        System.out.println("Vetor C: ");
        for (int i = 0; i < C.length; i++) {
            System.out.println("C["+i+"]"+ C[i]);
        }



        sc.close();
    }
}
