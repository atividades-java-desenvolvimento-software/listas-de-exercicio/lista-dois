import java.util.Scanner;

public class Exercicio03 {
    public void exercicioTres(){
        Scanner sc = new Scanner(System.in);
        /*
         * 3. Faça um programa que leia em um vetor uma sequência finita de números digitados 
        pelo  usuário.  Crie  um  segundo  vetor  que  armazene  o  dobro  de  cada  número  do 
        primeiro vetor e depois apresente os valores deste veto
         */
        int vetor[] = new int[5];

        for(int i = 0;i<5;i++){
            System.out.println("Informe vetor: ");
            vetor[i] = sc.nextInt();
        }
        int dobroVetor[] = new int[5];

        for(int i =0;i<vetor.length;i++){
            dobroVetor[i] = 2 * vetor[i];
        }

        System.out.println("Dobro do vetor: ");
        for(int i = 0;i<dobroVetor.length;i++){
            System.out.println(dobroVetor[i]);
        }

        sc.close();
    }
    
}
