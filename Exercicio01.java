import java.util.Scanner;

public class Exercicio01 {
    public void exercicioUm(){
        /*
         * 1. Construa um programa que leia em um vetor uma sequência de 5 números digitados 
            pelo usuário, calcule a média destes valores em um outro vetor, e depois apresente 
            na tela quais valores que são menores, iguais ou superiores à média.
         */
        Scanner sc = new Scanner(System.in);
        double vetor[] = new double[5];
        double soma = 0;

        for(int i = 0;i<5;i++){
            System.out.println("Informe nota: ");
            vetor[i] = sc.nextDouble();
            soma += vetor[i];
        }

        double media = soma / 5;

        System.out.println("Média: " + media);

        for(int i = 0; i<5; i++){
            if (vetor[i] < media) {
                System.out.println("Valor: " + vetor[i] + " é menor que a média");
            }else if(vetor[i] == media){
                System.out.println("Valor: " + vetor[i] + " é igual a média");
            }else{
                System.out.println("Valor: " + vetor[i] + " é maior que a média");
            }
        }
        sc.close();
    }
}
