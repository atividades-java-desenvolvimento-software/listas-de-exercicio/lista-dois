import java.util.Scanner;

public class Exercicio15 {
    public void exercicioQuinze(){

        Scanner sc = new Scanner(System.in);

        
        int[] vetor = new int[5];

        
        System.out.println("Digite o primeiro número: ");
        vetor[0] = sc.nextInt();
        int anterior = vetor[0]; // Guarda o número anterior

        for (int i = 1; i < vetor.length; i++) {
            System.out.println("Digite o próximo número: ");
            int numero = sc.nextInt();
            if (numero > anterior) {
                vetor[i] = numero;
                anterior = numero;
            } else {
                System.out.println("Número inválido. Digite um número maior que " + anterior + ".");
                i--; // Permanece na mesma posição para que o usuário digite novamente
            }
        }

        System.out.println("Vetor preenchido:");
        for (int num : vetor) {
            System.out.print(num + " ");
        }
        System.out.println();

        sc.close();
    }
}
