import java.util.Arrays;
import java.util.Scanner;

public class Exercicio08 {
    public void exercicioOito(){
        Scanner sc = new Scanner(System.in);

        int[] A = {1,2,3,4,5};
        int[] B = {6,7,8,9,10};
        //declarando C com tamnho de A & B
        int[] C = new int[A.length + B.length];

        //passando elementos de A pra C
        for (int i = 0; i < A.length; i++) {
            C[i] = A[i];
        }

        //passando elementos de B pra C, começando por onde A terminou
        for (int i = 0; i < B.length; i++) {
            C[A.length + i] = B[i];
        }

        //imprimindo C
        System.out.println("CONCATENAÇÃO:" + Arrays.toString(C));


        sc.close();
    }
}
