import java.util.Scanner;

public class Exercicio07 {
    public void exercicioSete(){
        Scanner sc = new Scanner(System.in);

        //vetor 1 e 2;
        int vetUm[] = new int[5];
        int vetDois[] = new int[5];

        for(int i = 0;i<vetUm.length;i++){
            System.out.println("Informe Vetor 1: ["+i+"]" );
            vetUm[i] = sc.nextInt();
        }

        System.out.println("----------------------------------");

        for(int i = 0;i<vetDois.length;i++){
            System.out.println("Informe Vetor 2: ["+i+"]" );
            vetDois[i] = sc.nextInt();
        }

        for(int i = 0;i<5;i++){
            if (vetUm[i] == vetDois[i]) {
                System.out.println("Todos os indices dos dois vetores são iguais");
            }else{
                System.out.println("Os indices não são iguais");
            }
        }

        sc.close();
    }
}
