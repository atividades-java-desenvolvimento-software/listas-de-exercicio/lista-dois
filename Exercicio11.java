import java.util.Scanner;

public class Exercicio11 {
    public void exercicioOnze(){
        Scanner sc = new Scanner(System.in);

        int A[] = new int[5];
        int B[] = new int[5];

        for (int i = 0; i < A.length; i++) {
            System.out.println("Informe Vetor A: ");
            A[i] = sc.nextInt();
        }
        for (int i = 0; i < B.length; i++) {
            System.out.println("Informe Vetor B: ");
            B[i] = sc.nextInt();
        }

        int produtoEscalar = 0;

        for (int i = 0; i < A.length; i++) {
            produtoEscalar += A[i] * B[i];
        }

        System.out.println("Produto Escalar: " + produtoEscalar);

        sc.close();
    }
    
}
