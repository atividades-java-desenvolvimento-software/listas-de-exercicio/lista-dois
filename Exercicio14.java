import java.util.Scanner;

public class Exercicio14 {
    public void exercicioQuatorze(){
        Scanner sc = new Scanner(System.in);

        int vetor[] = new int[5];

        for (int i = 0; i < vetor.length; i++) {
            System.out.println("Informe elemento: ");
            vetor[i] = sc.nextInt();
        }

        int maior =0;
        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] > vetor[maior]) {
                maior = i;
            }
        }

        int temp = vetor[maior];
        vetor[maior] = vetor[vetor.length - 1];
        vetor[vetor.length - 1] = temp;

        System.out.println("maior elemento na ultima posição: ");
        for (int i = 0; i < vetor.length; i++) {
            System.out.println(vetor[i] + " ");
        }

        sc.close();
    } 
}
