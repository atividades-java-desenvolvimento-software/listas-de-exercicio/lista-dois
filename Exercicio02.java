import java.util.Scanner;

public class Exercicio02 {
    public void exercicioDois(){
        /*
         * 2.  Escreva  um  algoritmo  que  leia  em  um  vetor  uma  sequência  finita  de  números 
            digitados pelo usuário e, logo após, mostre o número de cada posição do vetor e se 
            ele é positivo, negativo ou zero
        */
        Scanner sc = new Scanner(System.in);
        
        int numero[] = new int[5];

        for(int i = 0;i<5; i++){
            System.out.println("Informe vetor" + "[" + i + "]:" );
            numero[i] = sc.nextInt();
        }

        for(int i = 0;i<5;i++){
            if(numero[i] < 0){
                System.out.println("Número: " + numero[i] + " - é negativo " + "Posição: " + i );
            }else if(numero[i] > 0){
                System.out.println("Número: " + numero[i] + " - é positivo " + " Posição: " + i );
            }else{
                System.out.println("Número: " + numero[i] + " - é Zero " + " Posição: " + i );
            }
        }

        sc.close();
    }
}
