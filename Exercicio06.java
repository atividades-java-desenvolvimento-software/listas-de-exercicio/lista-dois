import java.util.Scanner;

public class Exercicio06 {
    public void exercicioSeis(){
        Scanner sc = new Scanner(System.in);
        //ler vetor com 5 notas:
        double vetor[] = new double[5];

        for(int i = 0;i<vetor.length;i++){
            System.out.println("Informe nota: ["+(i+1)+"]" );
            vetor[i] = sc.nextDouble();
        }

        //ler pesos
        double pesos[] = new double[5];

        for(int i = 0;i<vetor.length;i++){
            System.out.println("Informe peso: ["+(i+1)+"]" );
            pesos[i] = sc.nextDouble();
        }
        //inicia uma variavel de soma iniciando em 0;
        double somaPonderada = 0;
        double somaPesos = 0;
        //efetua os calculos de soma e multiplicação
        for(int i = 0;i<vetor.length;i++){
            somaPonderada += vetor[i] * pesos[i];
            somaPesos += pesos[i];
        }
        // faz a divisão das somas(media ponderada)
        double mediaPonderada = somaPonderada / somaPesos;

        System.out.println("Média ponderada: " + mediaPonderada);

        sc.close();
    }
}
