import java.util.Scanner;

public class Exercicio13 {
    public void exercicioTreze(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe a sequencia de Fibonacci: ");
        int n = sc.nextInt();

        if (n<=0) {
            System.out.println("O número de termos deve ser maior que 0");
        }else{
            int fibo[] = new int[n];
            // calculando os termos
            fibo[0] = 1; // primeiro termo
            if (n>1) {
                fibo[1] = 1; // segundo
                for(int i = 2;i<n;i++){
                    fibo[i] = fibo[i-1] + fibo[i-2];
                }
            }

            System.out.println("Os " + n + "primeiros termos da sequencia de fibonnaci: ");
            for (int i = 0; i < n; i++) {
                System.out.println(fibo[i] + " ");
            }
        }




        sc.close();
    }
}
